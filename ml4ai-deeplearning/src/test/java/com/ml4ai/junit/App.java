package com.ml4ai.junit;

import com.ml4ai.nn.core.Variable;
import org.junit.Test;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

public class App {

    @Test
    public void testReshape() {
        INDArray x = Nd4j.randn(4, 4);
        System.out.println(x.reshape(-1, 2));
    }

    @Test
    public void testMath() {
        double a = Math.log(Math.E);
        System.out.printf("a=" + a);
    }

    @Test
    public void testLog() {
        Variable var = new Variable(Nd4j.create(new double[]{Math.pow(Math.PI, 7D)}));
        System.out.println(var.log(Math.PI).data.tensor);
    }

}
