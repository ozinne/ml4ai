import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ModelGraphic from './ModelGraphic';
import {Button} from 'antd';

class App extends Component {
  render() {
    return (
      <div className="App" style={{width:'100%',height:'100%'}}>
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
      <ModelGraphic/>
        <p className="App-intro">

        </p>
      </div>
    );
  }
}

export default App;
