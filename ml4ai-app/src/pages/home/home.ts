import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  flag = false;

  constructor(public navCtrl: NavController) {

  }

  switchFlag() {
    this.flag = !this.flag;
  }

}
