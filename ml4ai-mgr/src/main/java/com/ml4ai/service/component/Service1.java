package com.ml4ai.service.component;

import org.springframework.stereotype.Component;

@Component
public class Service1 {

    public void hello(String hi) {
        System.out.println("hello " + hi);
    }

}
