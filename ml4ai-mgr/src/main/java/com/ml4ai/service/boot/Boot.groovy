package com.ml4ai.service.boot

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer
import org.springframework.context.annotation.ComponentScan

@EnableDiscoveryClient
@EnableEurekaClient
@EnableEurekaServer
@EnableAutoConfiguration
@SpringBootApplication
@ComponentScan("com.ml4ai.service")
class Boot {

    static void main(String[] args) {
        def springApp = new SpringApplication(Boot.class)
        springApp.run(args)
    }

}
