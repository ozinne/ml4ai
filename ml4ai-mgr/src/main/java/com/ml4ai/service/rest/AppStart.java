package com.ml4ai.service.rest;

import com.ml4ai.service.utils.CloseUtils;
import com.ml4ai.service.utils.JSONUtil;
import com.ml4ai.service.utils.ObjUtil;
import com.ml4ai.service.utils.SpringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

@RestController
public class AppStart {

    @RequestMapping("/ping")
    public String hello() {
        return "hello";
    }

    @RequestMapping("/execute")
    public String execute(String service, String method, String object) {
        try {
            Object parameter = ObjUtil.str2obj(object);
            Class<?> clazz = Class.forName(service);
            Object instance = SpringUtils.getService(clazz);
            Method[] methods = clazz.getMethods();
            for (Method m : methods) {
                if (m.getName().equals(method) && m.getParameterCount() == 1) {
                    Object returnVal = m.invoke(instance, parameter);
                    Map<String, String> response = new HashMap<>();
                    response.put("code", "200");
                    response.put("data", ObjUtil.obj2str(returnVal));
                    return JSONUtil.toJson(response);
                } else {
                    continue;
                }
            }
        } catch (Exception e) {
            Map<String, String> response = new HashMap<>();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PrintStream out = new PrintStream(baos);
            e.printStackTrace(out);
            response.put("code", "500");
            response.put("data", new String(baos.toByteArray()));
            CloseUtils.close(out);
            CloseUtils.close(baos);
            return JSONUtil.toJson(response);
        }
        Map<String, String> response = new HashMap<>();
        response.put("code", "400");
        response.put("data", "资源找到");
        return JSONUtil.toJson(response);
    }

}
