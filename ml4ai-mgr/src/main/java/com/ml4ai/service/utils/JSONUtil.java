package com.ml4ai.service.utils;

import com.google.gson.Gson;

public class JSONUtil {

    private static Gson defaultGson = new Gson();

    public static String toJson(Object object) {
        return defaultGson.toJson(object);
    }

}
