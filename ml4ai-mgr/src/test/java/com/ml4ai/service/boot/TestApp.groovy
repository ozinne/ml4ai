package com.ml4ai.service.boot

import com.ml4ai.service.utils.ObjUtil
import org.junit.Test

class TestApp {

    @Test
    void test() {
        print(ObjUtil.obj2str("你好"))
        print("\n")
        print(ObjUtil.obj2str(null))
        print("\n")
        print(ObjUtil.str2obj(ObjUtil.obj2str(null)))
        print("\n")
        print(ObjUtil.str2obj(ObjUtil.obj2str(1)).getClass())
        print("\n")
        print(Object.class.isAssignableFrom(String.class))
        print("\n")
        print(Integer.class.isAssignableFrom(int.class))

    }

}
